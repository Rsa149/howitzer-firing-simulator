package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import howitzer.AirSimulator;
import howitzer.Projectile;
import howitzer.WaterSimulator;
import howitzer.ThickLiquidSimulator;

/**
 * SystemTesting -  Test System as a whole and test proper interaction between the classes
 * 
 * 
 */
public class SystemTesting {

    double mass;
    double[] Position;
    double[] Velocity;
    double[] gravity;
    double[] force;
    double dragCoefficient;
    double fluidDensity;
    double[] flowVelocity;
    double radius;

    /**
     * setup - setup the initial values for the test
     * @throws Exception
     */
    @Before
    public void setup()throws Exception{
    // Create a Projectile with valid input values
        mass = 1.0;
        Velocity =  new double[] { 10.0, 0.0, 0.0 };
        Position = new double[]  { 0.0, 0.0, 0.0 };
        gravity =  new double[] { 0.0, 0.0, -9.81 };
        force =  new double[] { 0.0, 0.0, 0.0 };
        dragCoefficient = 0.47;

        flowVelocity =  new double[] { 1.0, 0.0, 0.0 };
        radius = 1.0;
    }

    @After
	public void tearDown() throws Exception {
	}

    /**
     * testSystem - test the interaction between the Projectile class and the different simulator classes
     * 
     */
    @Test
    public void testSystem(){
        fluidDensity = 1.225;
        AirSimulator airSimulator = new AirSimulator();
        Projectile resultAir = airSimulator.runSimulator(Position, Velocity, mass, flowVelocity, force, radius, dragCoefficient, fluidDensity, gravity);
        assertNotNull(resultAir);
        assertArrayEquals(Velocity, resultAir.getVelocity(), 0.0001);
        assertArrayEquals(Position, resultAir.getPosition(), 0.0001);

        fluidDensity = 10.0;
        WaterSimulator waterSimulator = new WaterSimulator();
        Projectile resultWater = waterSimulator.runSimulator(Position, Velocity, mass, flowVelocity, force, radius, dragCoefficient, fluidDensity, gravity);
        assertNotNull(resultWater);
        assertArrayEquals(Velocity, resultWater.getVelocity(), 0.0001);
        assertArrayEquals(Position, resultWater.getPosition(), 0.0001);

        fluidDensity = 40.0;
        ThickLiquidSimulator thickLiquidSimulator = new ThickLiquidSimulator();
        Projectile resultThickLiquid = thickLiquidSimulator.runSimulator(Position, Velocity, mass, flowVelocity, force, radius, dragCoefficient, fluidDensity, gravity);
        assertNotNull(resultThickLiquid);
        assertArrayEquals(Velocity, resultThickLiquid.getVelocity(), 0.0001);
        assertArrayEquals(Position, resultThickLiquid.getPosition(), 0.0001);
    }

}
