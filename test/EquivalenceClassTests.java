package test;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import howitzer.Projectile;

/**
 * Equivalence class testing for Projectile class
 * 
 */
public class EquivalenceClassTests {
    Projectile projectile;
    private double[] validInitialVelocity;
    private double[] validGravity;
    private double[] validForce;
    private double[] validFlowVelocity;

    private double[] invalidInitialVelocity;
    private double[] invalidGravity;
    private double[] invalidForce;
    private double[] invalidFlowVelocity;
    private double mass;
    private double radius;
    private double dragCoefficient;
    private double fluidDensity;
    private double[] initialPosition;

    @Before
    public void setup()throws Exception{
        //Valid inputs values for the projectile
        validInitialVelocity = new double[] {10.0, 30.0, 40.0};        // Valid non-zero initial velocity with 3 dimensions
        validGravity = new double[] {0.0, 0.0, -9.81};                 // Valid non-zero gravity with 3 dimensions
        validForce = new double[] {5.0, 2.0, 0.0};                     // valid force with 3 dimensions
        validFlowVelocity = new double[] {1.0, 1.0, 0.0};               // valid flow velocity with 3 dimensions

        //Invalid input values for the projectile
        invalidInitialVelocity = new double[] {0.0, 0.0, 0.0};          //initial velocity should not be zero
        invalidGravity = new double[] {0.0, 0.0,0.0 };                  // gravity should not be zero
        invalidForce = new double[] {10.0, 2.0};                        // invalid dimensions (other than 3)
        invalidFlowVelocity = new double[] {-1.0, 0.0};                 // invalid dimensions (other than 3)

        mass = 1;
        radius = 1;
        dragCoefficient = 0.47;
        fluidDensity = 1000.0;
        initialPosition = new double[] {1.0, 2.0, 3.0};
    }

    @After
    public void tearDown() throws Exception {
	}

    /**
     * testValidEquivalenceClass - test valid equivalence class
     * 
     * Throws an IllegalArgumentException if wrong.
     */ 
    @Test
    public void testValidEquivalenceClass(){
        try{
            projectile = new Projectile(mass, validInitialVelocity, initialPosition,validGravity, validForce, dragCoefficient, fluidDensity, validFlowVelocity, radius);
            assertNotNull(projectile);

            assertArrayEquals(projectile.getVelocity(), validInitialVelocity, 0.0001);     //Allow tolerance of 0.0001
            assertArrayEquals(projectile.getGravity(), validGravity, 0.0001);
            assertArrayEquals(projectile.getForce(), validForce, 0.0001);
            assertArrayEquals(projectile.getFlowVelocity(), validFlowVelocity, 0.0001);

        } catch (IllegalArgumentException e) {
            fail("Expected no IllegalArgumentException to be thrown");
        }   
    }

    /**
     * testInvalidEquivalenceClass - test invalid equivalence class
     * 
     * Throws an IllegalArgumentException if wrong values are passed.
     */
    @Test
    public void testInvalidEquivalenceClass(){
        //Invalid initial velocity
        try{
            projectile = new Projectile(mass, invalidInitialVelocity, initialPosition,validGravity, validForce, dragCoefficient, fluidDensity, validFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Velocity must not be a zero vector");
        }

        //Invalid gravity
        try{
            projectile = new Projectile(mass, validInitialVelocity, initialPosition,invalidGravity, validForce, dragCoefficient, fluidDensity, validFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Gravity must not be a zero vector");
        }

        //Invalid force
        try{
            projectile = new Projectile(mass, validInitialVelocity, initialPosition,validGravity, invalidForce, dragCoefficient, fluidDensity, validFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Force must have 3 dimensions");
        }

        //Invalid flow velocity
        try{
            projectile = new Projectile(mass, validInitialVelocity, initialPosition,validGravity, validForce, dragCoefficient, fluidDensity, invalidFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Flow velocity must have 3 dimensions");
        }
    }
}
