# Testing Report - 'Howitzer' Project

## 1. Introduction

The howitzer project aims to simulate the behavior of a projectile under different fluid conditions using various simulators (AirSimulator, WaterSimulator, and ThickLiquidSimulator). The project consists of several classes, including Projectile, AirSimulator, WaterSimulator, and ThickLiquidSimulator. Here AirSimulator is our version 1.0, WaterSimulator and ThickLiquidSimulator are our version 2.0.

This testing report provides an overview of the testing efforts undertaken for the howitzer project. The testing approach involved various testing techniques to ensure the correctness, robustness, and reliability of the code.

## 2. Testing Approach

The testing approach for the howitzer project involved the following techniques:

- Boundary Value Testing
- Equivalence Cases Testing
- Decision Table Testing
- Path Testing
- Data Flow Testing
- Integration Testing
- System Testing

## 3. Test Cases details

### 3.1 Boundary Value Testing

- Mass Boundary : Tested With MIN_MASS, MAX_MASS and valid values near the boundaries.
- Radius Boundary: Tested with MIN_RADIUS, MAX_RADIUS, and valid values near the boundaries.
- Drag Coefficient Boundary: Tested with MIN_DRAG_COEFFICIENT, MAX_DRAG_COEFFICIENT, and valid values near the boundaries.
- Fluid Density Boundary: Tested with MIN_FLUID_DENSITY, MAX_FLUID_DENSITY, and valid values near the boundaries.
- Vector Value Boundary: Tested with valid and invalid vector values, including NaN and Infinity.
- Vector Length Boundary: Tested with vectors of length 3, 2, and 1. The length should be 3.

#### The program code throws an exception when the input is invalid and not in the boundary


### 3.2 Equivalence Cases Testing

#### 3.2.1 Valid Cases

- Valid Initial Velocity: The validInitialVelocity array contains a non-zero 3-dimensional velocity vector {10.0, 30.0, 40.0}
- Valid Gravity: The validGravity array contains a non-zero 3-dimensional gravity vector {0.0, 0.0, -9.81}
- Valid Force: The validForce array contains a 3-dimensional force vector such as {5.0, 2.0, 0.0}
- Valid Flow Velocity: The validFlowVelocity array contains a 3-dimensional flow velocity vector such as{1.0, 1.0, 0.0}
- Mass: The mass variable should be between MIN_MASS and MAX_MASS
- Radius : The radius variable should be between MIN_RADIUS and MAX_RADIUS
- Drag Coefficient: The dragCoefficient variable should be between MIN_DRAG_COEFFICIENT and MAX_DRAG_COEFFICIENT
- Fluid Density: The fluidDensity variable should be between MIN_FLUID_DENSITY and MAX_FLUID_DENSITY
- Initial Position: The initialPosition variable should be a 3-dimensional vector

#### 3.2.2 Invalid Cases

- Invalid Initial Velocity: The invalidInitialVelocity array contains a 3-dimensional zero velocity vector {0.0, 0.0, 0.0}. Initial velocity should not be zero
- Invalid Gravity: The invalidGravity array contains a 3-dimensional zero gravity vector {0.0, 0.0, 0.0}. Gravity should not be zero
- InvlidForce, and InvalidFlowVelocity should be 3-dimensional vectors

### 3.3 Decision Table Testing

The DecisionTableTesting class demonstrates decision table testing for the Projectile class. It defines four conditions to be tested: mass within the valid range, radius within the valid range, presence of NaN values in vectors, and correct vector lengths. The decision table lists all possible combinations of these conditions and their expected outputs, helping to verify the correctness of the projectile's behavior under different scenarios.

### 3.4 Path Testing

Test Path for Valid Path Testing in Projectile class goes in the following order:

1. Valid Initialization of Projectile variables
2. Valid Update call
3. this requirs valid Total Force Calculation
4. requires valid Drag Force Calculation
5. requires valid Area Calculation
6. print updated position and velocity

This test path ensures that the projectile is initialized correctly, and the update function is called correctly. The update function further calls the total force calculation. The total force calculation calls the drag force calculation, which calls the area calculation. At the end of the update function, the updated position and velocity are printed.

This test path has code coverage of 90% for the Projectile class.

### 3.5 Data Flow Testing

The DataFlowTesting class demonstrates data flow testing for the Projectile class. We use DU Pair (Define and Use Pair) to test the data flow. This testing ensure that the variables are defined and used correctly in the code. 

### 3.6 Integration Testing

The IntegrationTesting class demonstrates integration testing for the Projectile class. It tests the integration between the Projectile class and the AirSimulator class. The test case initializes the projectile and the air simulator, and then calls runSimulation() on the air simulator. The air simulator calls the update() function on the projectile, which updates the position and velocity of the projectile. The test case then tests the updated position and velocity of the projectile to ensure that the integration between the projectile and the air simulator is correct. The same test case is used to test the integration between the projectile and the water simulator, and the projectile and the thick liquid simulator.

### 3.7 System Testing

The SystemTesting class demonstrates system testing for the howitzer project. It tests the system as a whole, including the projectile, the air simulator, the water simulator, and the thick liquid simulator. The test case initializes the projectile and the simulators, and then calls runSimulation() on the simulators. The simulators call the update() function on the projectile, which updates the position and velocity of the projectile. The test case then tests the updated position and velocity of the projectile to ensure that the system is working correctly. 