# Howitzer Firing Simulator

 A simulator to teach the soldiers about kinematics and dynamics

## Group Members

- Ramanpreet Singh
- Tirth Patel (200435378)

## Problem specifications

We are designing a simulator that instructs soldiers about the kinematics and dynamics required in using a howitzer. The simulator intends to offer soldiers a realistic training environment where they may learn about the variables that affect a projectile fired from a howitzer's trajectory and final position.

## Design Requirement

### Objective

 The specific objectives of the project are to develop a simulator that accurately simulates the firing of a projectile across an empty field and allows soldiers to interact with various parameters. These parameters include the barrel pose (position and orientation), radius and mass of the projectile, drag coefficient, initial speed, force applied to the projectile, and gravity. By manipulating these variables, soldiers should be able to observe the resulting changes in the trajectory and understand their impact. 

### Functions

- Calculate and display the trajectory variables, including the initial position, final position, and intermediate points.
- Enable customization of projectile parameters, such as radius, mass, drag coefficient, initial speed, and applied force.
- Generate informative visualizations and feedback to help soldiers understand the impact of different variables on the projectile's path.
- Offer interactive controls for soldiers to adjust parameters and observe the resulting trajectory changes.

### Constraints

- **Safety :** The simulator must prioritize safety by providing a virtual training environment that minimizes the risk of accidents or injuries.

- **Accessibility :** The design should ensure accessibility to a wide range of users, including soldiers with diverse abilities and learning styles.

- **Reliability :** The system should be resilient to potential failures and have mechanisms in place for error detection and recovery.

- **Economic Factors :** The simulator should be cost-effective in terms of development, maintenance, and deployment.


## Installation

To use the Howitzer Projectile Simulator, you need to have Java Development Kit (JDK) installed on your system. You can download the latest version of JDK from the [Oracle website](https://www.oracle.com/java/technologies/javase-downloads.html).

Once you have installed JDK, you can download or clone the Howitzer Projectile Simulator from this repository. You can then run the simulator by executing the following command : Java Simulator

## Usage

The Howitzer Projectile Simulator is a command-line application that allows you to simulate the firing of a projectile from a howitzer. The simulator prompts you to enter the projectile parameters, such as mass, radius, drag coefficient, initial speed, and applied force. It then calculates the trajectory of the projectile and displays the initial position, final position, and intermediate points. see Testing.md for more information about the accepatble values for the parameters.

```
        double mass = 1.0;
        double[] initialVelocity = { 10.0, 0.0, 0.0 };
        double[] initialPosition = { 0.0, 0.0, 0.0 };
        double[] gravity = { 0.0, 0.0, -9.81 };
        double[] force = { 0.0, 0.0, 0.0 };
        double dragCoefficient = 0.47;
        double fluidDensity = 1.225;
        double[] flowVelocity = { 1.0, 0.0, 0.0 };
        double radius = 1.0;

        Projectile projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, radius);

        // Run simulation for 10 seconds
        // Update the projectile position and velocity every 0.1 seconds
```

## Testing

Check out the [Testing.md](https://gitlab.com/Rsa149/howitzer-firing-simulator/-/blob/main/Documentation/Testing.md?ref_type=heads) file for more information about the testing process.


